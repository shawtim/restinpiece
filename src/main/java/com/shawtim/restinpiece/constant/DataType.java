package com.shawtim.restinpiece.constant;

import javax.ws.rs.core.MediaType;

public class DataType {

	public static final MediaType APPLICATION_UTF8_XML = MediaType.valueOf("application/xml; charset=utf-8");
	public static final MediaType APPLICATION_UTF8_JSON = MediaType.valueOf("application/json; charset=utf-8");
}
