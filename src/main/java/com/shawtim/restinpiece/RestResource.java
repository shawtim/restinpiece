/**
 * Licensed Materials - Property of IBM
 * 5724-Y68
 * (C) Copyright IBM Corp. 2009, 2010. All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

package com.shawtim.restinpiece;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONException;

import com.shawtim.restinpiece.exception.InvalidCSRFException;
import com.shawtim.restinpiece.exception.NotAuthorizedException;
import com.shawtim.restinpiece.exception.ResourceNotFoundException;
import com.shawtim.restinpiece.model.ErrorCode;
import com.shawtim.restinpiece.model.RestJSONObject;
import com.shawtim.restinpiece.model.RestObject;
import com.shawtim.restinpiece.util.JSONUtil;
import com.shawtim.restinpiece.util.RestResponseUtil;

/**
 * @author timliu
 *
 */

public class RestResource {
	
	@Context
	public HttpServletRequest request;
	@Context
	public HttpServletResponse response;
	
	protected Status responseStatus = Status.OK;
	protected RestObject restObject;
	protected MediaType mediaType;
	
	public void init() throws Throwable {
		init(MediaType.APPLICATION_JSON_TYPE);
	}
	
	public void init(MediaType mediaType) throws JSONException {
		
		if (MediaType.APPLICATION_JSON_TYPE.equals(mediaType)) {
			this.restObject = new RestJSONObject();
			this.mediaType = mediaType;
		}
	}
	
	public void validate() throws Throwable {
	}

	protected JSONArray getRequestJSON(String jsonString) throws JSONException {
		return JSONUtil.parseJSONArray(jsonString);
	}
	
	public void setErrorResponseStatus(Throwable t) {
		if (t instanceof WebApplicationException) {
			setErrorResponseStatus(Status.SERVICE_UNAVAILABLE);
		} else if (t instanceof JSONException) {
			setErrorResponseStatus(Status.SERVICE_UNAVAILABLE);
		} else if (t instanceof InvalidCSRFException) {
			setErrorResponseStatus(Status.FORBIDDEN);
		} else if (t instanceof ResourceNotFoundException) {
			setErrorResponseStatus(Status.NOT_FOUND);
		} else if (t instanceof NotAuthorizedException) {
			setErrorResponseStatus(Status.FORBIDDEN);
		} else {
			setErrorResponseStatus();
		}
	}
	
	public void setErrorResponseStatus() {
		responseStatus = Response.Status.Family.SUCCESSFUL.equals(responseStatus.getFamily()) ? Status.BAD_REQUEST : responseStatus;
	}
	
	public void setErrorResponseStatus(Status status) {
		responseStatus = status;
	}
	
	public void setErrorObject(Throwable t) {
		if (!RestResponseUtil.NO_RESPONSE_BODY_STATUS.contains(responseStatus)) {
			setErrorObject(ErrorCode.UNKNOWN, t.getMessage());
		}
	}
	
	public void setErrorObject(ErrorCode code, String msg, Object... args) {
		if (restObject != null) {
			restObject.setStatus(RestObject.Status.ERROR);
			restObject.setMessage(code, msg, args);
			restObject.clearEntry();
		}
	}
	
	public Response generateResponse() {
		return RestResponseUtil.response(responseStatus, restObject);
	}
}
