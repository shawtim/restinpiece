/**
 * Licensed Materials - Property of IBM
 * 5724-Y68
 * (C) Copyright IBM Corp. 2009, 2010. All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

package com.shawtim.restinpiece.util;

import java.util.List;

public class CommonUtil {
	
	public static final String STRING_SEPERATOR = ",";
	
	public static String join(List<String> list) {
		return join(list, STRING_SEPERATOR);
	}
	
	public static String join(List<String> list, String seperator) {
		
		if (list == null) return null;
		
		String joinStr = "";
		StringBuilder sb = new StringBuilder();
		
		for (String str : list) {
			sb.append(seperator);
			sb.append(str);
		}
		
		if (sb.length() > 0) {
			joinStr = sb.substring(seperator.length());
		}
		
		return joinStr;
	}
	
	public static boolean isNull(String str) {
		return str == null || str.isEmpty();
	}
	
	public static boolean isNull(List<? extends Object> list) {
		return list == null || list.isEmpty();
	}
}
