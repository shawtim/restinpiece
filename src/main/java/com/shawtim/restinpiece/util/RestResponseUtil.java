/**
 * Licensed Materials - Property of IBM
 * 5724-Y68
 * (C) Copyright IBM Corp. 2009, 2010. All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

package com.shawtim.restinpiece.util;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;

import com.shawtim.restinpiece.constant.DataType;
import com.shawtim.restinpiece.model.RestJSONObject;
import com.shawtim.restinpiece.model.RestObject;

public class RestResponseUtil {
	
	private static final Status[] NO_RESPONSE_BODY_STATUS_LIST = new Status[] { Status.UNAUTHORIZED, Status.FORBIDDEN, Status.NOT_FOUND };
	public static final Set<Status> NO_RESPONSE_BODY_STATUS = new HashSet<Status>();
	
	static {
		for (Status status : NO_RESPONSE_BODY_STATUS_LIST) {
			NO_RESPONSE_BODY_STATUS.add(status);
		}
	}
	
	public static Response response(Status status, RestObject restObject) {
		
		Response response = null;

		if (restObject instanceof RestJSONObject) {
			try {
				response = responseJSON(status, (RestJSONObject) restObject);
			} catch (JSONException e) {
				response = response(Status.INTERNAL_SERVER_ERROR);
			}
		} else {
			// TODO
		}
		
		return response;
	}
	
	private static Response responseJSON(Status status, RestJSONObject json) throws JSONException {
		
		String jsonString = null;

		if (json != null) {
			jsonString = json.toJSON().toString();
		}
		
		return response(status, DataType.APPLICATION_UTF8_JSON, jsonString);
	}

	public static Response response(Status status) {
		return response(status, null, null);
	}

	public static Response response(Status status, MediaType mediaType, Object entity) {
		
		ResponseBuilder responseBuilder = Response.status(status);
		
		if (!NO_RESPONSE_BODY_STATUS.contains(status) && entity != null) {
			responseBuilder = responseBuilder.type(mediaType);
			responseBuilder = responseBuilder.entity(entity);
		}
		
		return responseBuilder.build();
	}
}
