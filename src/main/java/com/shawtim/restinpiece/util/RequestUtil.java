/**
 * Licensed Materials - Property of IBM
 * 5724-Y68
 * (C) Copyright IBM Corp. 2009, 2010. All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

package com.shawtim.restinpiece.util;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class RequestUtil {
	
	public static boolean hasParameter(HttpServletRequest request, String parameterName) {
		return request.getParameter(parameterName) != null;
	}
	
	public static String getStringParameter(HttpServletRequest request, String parameterName) {
		return request.getParameter(parameterName);
	}
	
	public static List<String> getStringParameters(HttpServletRequest request, String parameterName) {
		return Arrays.asList(request.getParameterValues(parameterName));
	}
	
	public static long getLongParameter(HttpServletRequest request, String parameterName) {
		String paramStr = getStringParameter(request, parameterName);
		return (paramStr != null) ? Long.valueOf(paramStr.trim()) : null;
	}
	
	public static int getIntParameter(HttpServletRequest request, String parameterName) {
		String paramStr = getStringParameter(request, parameterName);
		return (paramStr != null) ? Integer.valueOf(paramStr.trim()) : null;
	}
}
