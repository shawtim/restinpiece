package com.shawtim.restinpiece.util;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.shawtim.restinpiece.annotation.JSONOutput;
import com.shawtim.restinpiece.model.JSONable;

public class JSONUtil {

	/**
	 * given a json string, it parses the givn json string into a json array.
	 * @param jsonString json string
	 * @return json array from the string. if the given json string represents a json object, it returns json array with 1 element.
	 * @throws JSONException the given string is not a json string
	 */
	public static JSONArray parseJSONArray(String jsonString) throws JSONException {
		JSONArray jsonArray = new JSONArray();
		try {
			jsonArray = new JSONArray(jsonString);
		} catch (JSONException e) {
			jsonArray.put(new JSONObject(jsonString));
		}
		return jsonArray;
	}
	
	public static Object parseObject(Object object) throws JSONException {
		
		Object obj = null;

		if (object instanceof JSONable) {
			obj = ((JSONable) object).toJSON();
		} else if (object.getClass().isAnnotationPresent(com.shawtim.restinpiece.annotation.JSONObject.class)) {
			obj = parseJSONObjectPOJO(object);
		} else if (object.getClass().isArray()) {
			JSONArray array = new JSONArray();
			Object[] arr = (Object[]) object;
			for (Object o : arr) {
				array.put(parseObject(o));
			}
			obj = array;
		} else if (object instanceof Map) {
			JSONObject json = new JSONObject();
			Map<?, ?> map = (Map<?, ?>) object;
			for (Object key : map.keySet()) {
				json.put(key.toString(), parseObject(map.get(key)));
			}
			obj = json;
		} else if (object instanceof Iterable) {
			JSONArray array = new JSONArray();
			Iterable<?> it = (Iterable<?>) object;
			for (Object o : it) {
				array.put(parseObject(o));
			}
			obj = array;
		} else if (object instanceof Serializable) {
			obj = (Serializable) object;
		} else {
			obj = parseJSONOutputPOJO(object);
		}
		
		return obj;
	}
	
	private static JSONObject parseJSONObjectPOJO(Object object) {
		
		JSONObject json = new JSONObject();
		
		Field[] fields = object.getClass().getDeclaredFields();
		Method[] methods = object.getClass().getMethods();
		
		Map<String, Method> methodMap = new HashMap<String, Method>();
		for (Method method : methods) {
			String name = method.getName();
			if (!method.isVarArgs() && (name.startsWith("is") || name.startsWith("get"))) {
				methodMap.put(name.toLowerCase(), method);
			}
		}
		
		for (Field field : fields) {
			String name = field.getName().toLowerCase();
			String booleanName = "is" + name;
			String normalName = "get" + name;
			
			Method method = null;
			if (methodMap.containsKey(booleanName)) {
				method = methodMap.get(booleanName);
			} else if (methodMap.containsKey(normalName)) {
				method = methodMap.get(normalName);
			}
			
			if (method != null) {
				
				try {
					Object value = method.invoke(object);
					json.put(field.getName(), parseObject(value));
				} catch (Exception e) {
					// skip if exceptions
					e.printStackTrace();
				}
				
			}
		}
		
		return json;
	}
	
	private static JSONObject parseJSONOutputPOJO(Object object) {
		
		JSONObject json = new JSONObject();
		
		Method[] methods = object.getClass().getMethods();
		for (Method method : methods) {
			if (method.isAnnotationPresent(JSONOutput.class) && !method.isVarArgs()) {
				try {
					Object value = method.invoke(object);
					String key = method.getAnnotation(JSONOutput.class).value();
					if (value instanceof JSONable) {
						json.put(key, ((JSONable) value).toJSON());
					} else if (value instanceof Serializable) {
						json.put(key, (Serializable) value);
					}
				} catch (Exception e) {
					// skip and log
					e.printStackTrace();
				}
				
			}
		}
		
		return json;
	}
}
