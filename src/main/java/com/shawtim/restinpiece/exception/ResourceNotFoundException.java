/**
 * Licensed Materials - Property of IBM
 * 5724-Y68
 * (C) Copyright IBM Corp. 2009, 2010. All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

package com.shawtim.restinpiece.exception;

public class ResourceNotFoundException extends RestException {
	
	private static final long serialVersionUID = -3251134148086491217L;

	public ResourceNotFoundException() {
		super();
	}
}
