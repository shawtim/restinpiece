/**
 * Licensed Materials - Property of IBM
 * 5724-Y68
 * (C) Copyright IBM Corp. 2009, 2010. All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

package com.shawtim.restinpiece.exception;

public class NotAuthorizedException extends RestException {
	
	private static final long serialVersionUID = 2158462959310832397L;

	public NotAuthorizedException() {
		super();
	}
}
