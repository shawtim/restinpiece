/**
 * Licensed Materials - Property of IBM
 * 5724-Y68
 * (C) Copyright IBM Corp. 2009, 2010. All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

package com.shawtim.restinpiece.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.shawtim.restinpiece.util.JSONUtil;

public class RestJSONObject extends RestObject implements JSONable {

	protected JSONObject json = new JSONObject();
	
	public RestJSONObject() throws JSONException {
		json.put(KEY_ENTRY, new JSONArray());
	}
	
	public RestJSONObject(int startIndex, int itemsPerPage, int totalResults) throws JSONException {
		json.put(KEY_ENTRY, new JSONArray());
		setStartIndex(startIndex);
		setTotalResults(totalResults);
		setItemsPerPage(itemsPerPage);
		setStatus(Status.OK);
	}
	
	public JSONObject getJSONMessage() throws JSONException {
		return getMessage() == null ? null : new Message(getMessage()).toJSON();
	}

	public JSONObject toJSON() throws JSONException {
		
		json.put(KEY_STARTINDEX, getStartIndex());
		json.put(KEY_TOTALRESULTS, getTotalResults());
		json.put(KEY_ITEMSPERPAGE, getItemsPerPage());
		json.put(KEY_STATUS, getStatus().getValue());
		json.put(KEY_MESSAGE, getJSONMessage());
		
		JSONArray array = new JSONArray();
		for (Object data : getEntry()) {
			if (data instanceof JSONable) {
				array.put(((JSONable) data).toJSON());
			} else {
				array.put(JSONUtil.parseObject(data));
			}
		}
		json.put(KEY_ENTRY, array);
		
		return json;
	}

	public class Message extends RestObject.Message implements JSONable {
    	
    	protected Message(RestObject.Message message) {
    		super(message.code, message.msg, message.args);
    	}

		public JSONObject toJSON() throws JSONException {
			
    		JSONObject json = new JSONObject();
    		json.put("code", code.getValue());
    		json.put("msg", msg);
    		
    		JSONArray paramList = new JSONArray();
    		for (Object obj : params) {
    			paramList.put(obj.toString());
    		}
    		json.put("params", paramList);
    		
    		return json;
		}
    }
}
