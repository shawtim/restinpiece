/**
 * Licensed Materials - Property of IBM
 * 5724-Y68
 * (C) Copyright IBM Corp. 2009, 2010. All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

package com.shawtim.restinpiece.model;

import java.util.ArrayList;
import java.util.List;

public class RestObject {
	
	public static final String KEY_ENTRY = "entry";
	public static final String KEY_STARTINDEX = "startIndex";
	public static final String KEY_TOTALRESULTS = "totalResults";
	public static final String KEY_ITEMSPERPAGE = "itemsPerPage";
	public static final String KEY_STATUS = "status";
	public static final String KEY_MESSAGE = "message";
	
	public enum Status {
		OK("OK"), ERROR("ERROR"), INVALID("INVALID");
		private String value;
		private Status(String value) {
			this.value = value;
		}
		protected String getValue() {
			return value;
		}
		protected static Status getStatus(String value) {
			Status status = Status.INVALID;
			for (Status s : Status.values()) {
				if (s.getValue().equals(value)) {
					status = s;
				}
			}
			return status;
		}
	};
	
	protected List<Object> entries = new ArrayList<Object>();
	protected int startIndex = 1;
	protected int totalResults = 0;
	protected int itemsPerPage = 0;
	protected Status status = Status.OK;
	protected Message message = null;
	
	public List<Object> getEntry() {
		return entries;
	}
	public void addEntry(Object entry) {
		this.entries.add(entry);
	}
	public void clearEntry() {
		this.entries.clear();
	}
	public int getStartIndex() {
		return startIndex;
	}
	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}
	public int getTotalResults() {
		return totalResults;
	}
	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}
	public int getItemsPerPage() {
		return itemsPerPage;
	}
	public void setItemsPerPage(int itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Message getMessage() {
		return message;
	}
	public void setMessage(Message message) {
		this.message = message;
	}
	public void setMessage(ErrorCode code, String msg, Object... args) {
		setMessage(new Message(code, msg, args));
	}

    public class Message {
    	
    	public static final String KEY_CODE = "code";
    	public static final String KEY_MSG = "msg";
    	public static final String KEY_PARAMS = "params";
    	
    	protected ErrorCode code;
    	protected String msg;
    	protected Object[] args;
    	protected List<Object> params;
    	
    	protected Message(ErrorCode code, String msg, Object... args) {
    		this.code = code;
    		this.msg = msg;
    		this.args = args;
    		this.params = new ArrayList<Object>();
    		for (Object obj : args) {
    			this.params.add(obj);
    		}
    	}
    }
}
