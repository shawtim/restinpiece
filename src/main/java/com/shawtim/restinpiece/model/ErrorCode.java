/**
 * Licensed Materials - Property of IBM
 * 5724-Y68
 * (C) Copyright IBM Corp. 2009, 2010. All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

package com.shawtim.restinpiece.model;

public class ErrorCode {
	
	public static final ErrorCode UNKNOWN = new ErrorCode("UNKNOWN");
	
	private String value;
	
	public ErrorCode(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
