package com.shawtim.restinpiece.internal.servlet;

import javax.ws.rs.core.Application;

import org.apache.wink.server.internal.DeploymentConfiguration;
import org.apache.wink.server.internal.servlet.RestServlet;

import com.shawtim.restinpiece.internal.RestDeploymentConfiguration;
import com.shawtim.restinpiece.internal.application.RestApplication;
import com.shawtim.restinpiece.util.CommonUtil;

public class RestApplicationServlet extends RestServlet {

	private static final long serialVersionUID = -1122047555352877816L;
	
	private static final String REST_PACAKGE_PARAM = "restPackageName";
	private static final String DEPLYMENT_CONF_PARAM = "deploymentConfiguration";
	
	@Override
	protected Application getApplication() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		Application application = null;
		
		String restPackageParam = getInitParameter(REST_PACAKGE_PARAM);
		if (CommonUtil.isNull(restPackageParam)) {
			//application = super.getApplication();
			application = new RestApplication("");
		} else {
			application = new RestApplication(restPackageParam);
		}
		
		return application;
	}
	
	@Override
	protected DeploymentConfiguration createDeploymentConfiguration() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		DeploymentConfiguration deploymentConfiguration = null;
		
		String initParameter = getInitParameter(DEPLYMENT_CONF_PARAM);
		if (initParameter != null) {
			deploymentConfiguration = super.createDeploymentConfiguration();
		} else {
			deploymentConfiguration = new RestDeploymentConfiguration();
		}
		
		return deploymentConfiguration;
	}
}
