package com.shawtim.restinpiece.internal.application;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.ws.rs.core.Application;

import com.shawtim.restinpiece.annotation.Rest;
import com.shawtim.restinpiece.util.CommonUtil;

public class RestApplication extends Application {
	
	private static final String CLASS_SUFFIX = ".class";
	
	private String packageName;
	private Class<? extends Annotation> restClass;
	
	public RestApplication(String packageName) {
		this(packageName, Rest.class);
	}
	
	public RestApplication(String packageName, Class<? extends Annotation> restClass) {
		this.packageName = packageName;
		this.restClass = restClass;
	}

	@Override
	public Set<Class<?>> getClasses() {
		
        Set<Class<?>> classes = new HashSet<Class<?>>();
        
        try {
        	classes.addAll(getClasses(packageName, restClass));
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        
        return classes;
	}

	private Set<Class<?>> getClasses(String packageName, Class<? extends Annotation> restClass) throws IOException, ClassNotFoundException {
		
		if (CommonUtil.isNull(packageName)) {
			return new HashSet<Class<?>>();
		}
		
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		String packagePath = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(packagePath);
		
        List<File> dirs = new ArrayList<File>();
        List<JarFile> jars = new ArrayList<JarFile>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            if (resource.getFile().startsWith("file:") && resource.getFile().indexOf("jar!") >= 0) {
            	String jarName = resource.getFile();
            	jarName = jarName.substring(5, jarName.indexOf("jar!")+3);
            	jars.add(new JarFile(jarName));
            } else {
                dirs.add(new File(resource.getFile()));
            }
        }
        
        Set<Class<?>> classes = new HashSet<Class<?>>();
        for (File dir : dirs) {
        	classes.addAll(getClasses(dir, packageName, restClass));
        }
        for (JarFile jar : jars) {
        	classes.addAll(getJarClasses(jar, packagePath, restClass));
        }
        
        return classes;
	}
	
    private static Set<Class<?>> getClasses(File dir, String packageName, Class<? extends Annotation> restClass) throws ClassNotFoundException {
    	
        Set<Class<?>> classes = new HashSet<Class<?>>();
        
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                classes.addAll(getClasses(file, packageName + "." + file.getName(), restClass));
            } else if (file.getName().endsWith(CLASS_SUFFIX)) {
            	Class<?> clazz = Class.forName(packageName + "." + file.getName().substring(0, file.getName().length() - CLASS_SUFFIX.length()));
            	
            	if (clazz.isAnnotationPresent(restClass)) {
                    classes.add(clazz);
            	}
            }
        }
        
        return classes;
    }
	
    private static Set<Class<?>> getJarClasses(JarFile jar, String packagePath, Class<? extends Annotation> restClass) throws ClassNotFoundException {
    	
        Set<Class<?>> classes = new HashSet<Class<?>>();
        
        Enumeration<JarEntry> entries = jar.entries();
        while (entries.hasMoreElements()) {
        	JarEntry entry = entries.nextElement();
            if (entry.getName().startsWith(packagePath) && entry.getName().endsWith(CLASS_SUFFIX)) {
        		String className = entry.getName().substring(0, entry.getName().length() - CLASS_SUFFIX.length());
            	Class<?> clazz = Class.forName(className.replace('/', '.'));
            	
            	if (clazz.isAnnotationPresent(restClass)) {
                    classes.add(clazz);
            	}
            }
        }
        
        return classes;
    }
}
