package com.shawtim.restinpiece.internal;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;

import org.apache.wink.server.handlers.RequestHandler;
import org.apache.wink.server.handlers.ResponseHandler;
import org.apache.wink.server.handlers.ResponseHandlersChain;
import org.apache.wink.server.internal.DeploymentConfiguration;
import org.apache.wink.server.internal.handlers.FlushResultHandler;

import com.shawtim.restinpiece.internal.handlers.CleanUpRequestHandler;
import com.shawtim.restinpiece.internal.handlers.InvokeMethodPreparationHandler;
import com.shawtim.restinpiece.internal.handlers.PostInvokeHandler;
import com.shawtim.restinpiece.internal.handlers.RestErrorHandler;

public class RestDeploymentConfiguration extends DeploymentConfiguration {

	@Override
	protected List<RequestHandler> initRequestUserHandlers() {
		
		List<RequestHandler> handlers = new ArrayList<RequestHandler>();
		handlers.add(new InvokeMethodPreparationHandler());
		
		return handlers;
	}

	@Override
	protected List<ResponseHandler> initResponseUserHandlers() {
		
		List<ResponseHandler> handlers = new ArrayList<ResponseHandler>();
		handlers.add(new PostInvokeHandler());
		handlers.add(new CleanUpRequestHandler());
		
		return handlers;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected ResponseHandlersChain initErrorHandlersChain() {
		
		ResponseHandlersChain handlersChain = new ResponseHandlersChain();
		
		Class<?>[] classes = new Class<?>[] { RestErrorHandler.class, CleanUpRequestHandler.class, FlushResultHandler.class };
		for (Class<?> clazz : classes) {
			try {
				ResponseHandler handler = createHandler((Class<ResponseHandler>) clazz);
				handlersChain.addHandler(handler);
			} catch (ClassCastException e) {
				// ignore and skip
			}
		}
		
		for (ResponseHandler responseHandler : getErrorUserHandlers()) {
			responseHandler.init(getProperties());
			handlersChain.addHandler(responseHandler);
		}
		
		return handlersChain;
	}
	
	private ResponseHandler createHandler(Class<? extends ResponseHandler> clazz) {
		
		ResponseHandler handler = null;
		
		try {
			handler = clazz.newInstance();
			handler.init(getProperties());
		} catch (InstantiationException e) {
			throw new WebApplicationException(e);
		} catch (IllegalAccessException e) {
			throw new WebApplicationException(e);
		}
		
		return handler;
	}
}
