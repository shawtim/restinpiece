package com.shawtim.restinpiece.internal.handlers;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.apache.wink.server.handlers.AbstractHandler;
import org.apache.wink.server.handlers.MessageContext;

import com.shawtim.restinpiece.RestResource;

public class CleanUpRequestHandler extends AbstractHandler {
	
	@Override
	public void handleResponse(MessageContext context) throws Throwable {
		
		HttpServletRequest request = context.getAttribute(HttpServletRequest.class);
		
		cleanInRequest(request, RestResource.class);
		cleanInRequest(request, MediaType.class);
	}
	
	private void cleanInRequest(HttpServletRequest request, Class<?> clazz) {
		
		if (request != null) {
			request.removeAttribute(clazz.getName());
		}
	}
}
