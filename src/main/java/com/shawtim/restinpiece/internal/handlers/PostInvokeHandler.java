package com.shawtim.restinpiece.internal.handlers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.wink.server.handlers.AbstractHandler;
import org.apache.wink.server.handlers.MessageContext;
import org.apache.wink.server.internal.handlers.SearchResult;

import com.shawtim.restinpiece.annotation.PostInvoke;

public class PostInvokeHandler extends AbstractHandler {
	
	@Override
	public void handleResponse(MessageContext context) throws Throwable {
		
        SearchResult searchResult = context.getAttribute(SearchResult.class);
        Object instance = searchResult.getResource().getInstance(context);
        Method method = searchResult.getMethod().getMetadata().getReflectionMethod();
        
        if (method.isAnnotationPresent(PostInvoke.class)) {
        	String methodName = method.getAnnotation(PostInvoke.class).value();
        	invokeMethod(instance, methodName);
        }
	}
	
	private void invokeMethod(Object instance, String methodName) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		instance.getClass().getMethod(methodName).invoke(instance);
	}
}
