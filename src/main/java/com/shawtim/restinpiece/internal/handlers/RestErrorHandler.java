package com.shawtim.restinpiece.internal.handlers;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.wink.server.handlers.AbstractHandler;
import org.apache.wink.server.handlers.MessageContext;

import com.shawtim.restinpiece.RestResource;

public class RestErrorHandler extends AbstractHandler {
	
	@Override
	public void handleResponse(MessageContext context) throws Throwable {
		
		if (context.getResponseEntity() instanceof Throwable) {
			
			Throwable t = (Throwable) context.getResponseEntity();
			HttpServletRequest request = context.getAttribute(HttpServletRequest.class);
			RestResource restResource = retrieveFromRequest(request, RestResource.class);
			MediaType mediaType = retrieveFromRequest(request, MediaType.class);
			
			setRestError(restResource, t);
			setRestErrorResponse(context, restResource, mediaType);
		}
	}
	
	@SuppressWarnings("unchecked")
	private <T> T retrieveFromRequest(HttpServletRequest request, Class<T> clazz) {
		
		T t = null;
		
		if (request != null) {
			t = (T) request.getAttribute(clazz.getName());
		}

		return t;
	}
	
	private void setRestError(RestResource restResource, Throwable t) {
		
		if (restResource != null) {
            restResource.setErrorResponseStatus(t);
            restResource.setErrorObject(t);
		}
	}
	
	private void setRestErrorResponse(MessageContext context, RestResource restResource, MediaType mediaType) {
		
		if (restResource != null) {
            
            Response response = restResource.generateResponse();
            
            context.setResponseStatusCode(response.getStatus());
            context.setResponseEntity(response.getEntity());

            if (mediaType == null) {
            	mediaType = MediaType.TEXT_PLAIN_TYPE;
            }
            
            context.setResponseMediaType(mediaType);
		}
	}
}
