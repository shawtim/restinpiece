package com.shawtim.restinpiece.internal.handlers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.apache.wink.server.handlers.AbstractHandler;
import org.apache.wink.server.handlers.MessageContext;
import org.apache.wink.server.internal.handlers.SearchResult;

import com.shawtim.restinpiece.RestResource;
import com.shawtim.restinpiece.annotation.PreInvoke;
import com.shawtim.restinpiece.annotation.Validate;

public class InvokeMethodPreparationHandler extends AbstractHandler {
	
	@Override
	public void handleRequest(MessageContext context) throws Throwable {
		
        SearchResult searchResult = context.getAttribute(SearchResult.class);
        Object instance = searchResult.getResource().getInstance(context);
        
        if (instance instanceof RestResource) {
        	
            RestResource restResource = (RestResource) instance;
            HttpServletRequest request = context.getAttribute(HttpServletRequest.class);
            MediaType mediaType = getMediaType(searchResult);
            
            initRestResource(restResource, mediaType);
            storeInRequest(request, RestResource.class, restResource);
            storeInRequest(request, MediaType.class, mediaType);

            Method method = searchResult.getMethod().getMetadata().getReflectionMethod();
            
            if (method.isAnnotationPresent(Validate.class)) {
            	restResource.validate();
            }
            
            if (method.isAnnotationPresent(PreInvoke.class)) {
            	String methodName = method.getAnnotation(PreInvoke.class).value();
            	invokeMethod(restResource, methodName);
            }
        }
	}
	
	private void initRestResource(RestResource restResource, MediaType mediaType) throws Throwable {
    	restResource.init(mediaType);
	}
	
	private MediaType getMediaType(SearchResult searchResult) {
		
		MediaType mediaType = MediaType.TEXT_PLAIN_TYPE;
		
		Set<MediaType> mediaTypes = searchResult.getMethod().getMetadata().getProduces();
		if (mediaTypes != null) {
			for (MediaType type : mediaTypes) {
				mediaType = type;
			}
		}
		
		return mediaType;
	}
	
	private void storeInRequest(HttpServletRequest request, Class<?> clazz, Object object) {
		
		if (request != null) {
	        request.setAttribute(clazz.getName(), object);
		}
	}
	
	private void invokeMethod(RestResource restResource, String methodName) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		restResource.getClass().getMethod(methodName).invoke(restResource);
	}
}
